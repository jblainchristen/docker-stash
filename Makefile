

build:
	docker build --tag dweomer/stash:3.6.1 --tag dweomer/stash:latest .

run: data
	docker run --volumes-from=stash-data --name=stash-webapp -d -p 7990:7990 -p 7999:7999 dweomer/stash 

data:
	docker run -d -v /var/atlassian/application-data/stash --name stash-data dweomer/stash-data

clean:
	docker rm -vf stash-data stash-webapp

.PHONY: build run data clean
